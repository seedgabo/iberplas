import { Api } from "../../providers/Api";
import { Component } from "@angular/core";
import { AlertController, NavController, NavParams, ToastController } from "ionic-angular";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  canOrder = false;
  rand = Math.random() * 1000;
  sliders: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public api: Api, public alert: AlertController) {}

  async ionViewDidLoad() {
    await this.api.ready;
    this.api
      .get(`users/${this.api.user.id}?with[]=roles`)
      .then((result: any) => {
        result.roles.forEach(rol => {
          if (rol.name == "Pedido APP") this.canOrder = true;
        });
      })
      .catch(err => {
        console.error(err);
      });

    this.sliders = await this.api.get("sliders");
  }

  ionViewWillEnter() {
    this.rand = Math.random() * 1000;
  }

  orderAdmin() {
    this.navCtrl.push("ProductosPage");
  }
}
