import { Component } from "@angular/core";
import { NavController, AlertController, LoadingController, ModalController, Events } from "ionic-angular";
import { Api } from "../../providers/Api";
import { DomSanitizer } from "@angular/platform-browser";
import { HomePage } from "../home/home";
declare var window: any;
@Component({
  templateUrl: "login.html"
})
export class LoginPage {
  prefilled = false;
  backimg;
  constructor(
    public navController: NavController,
    public api: Api,
    public alert: AlertController,
    public loading: LoadingController,
    public modal: ModalController,
    public events: Events,
    private sanitizer: DomSanitizer
  ) {
    if (window.url) {
      this.prefilled = true;
      this.backimg = this.sanitizer.bypassSecurityTrustStyle(`url('${this.api.url}img/fondo (1).jpg')`);
    } else {
      this.api.url = "";
      this.backimg = this.sanitizer.bypassSecurityTrustStyle(`/assets/img/background-login.jpg`);
    }
  }

  doLogin() {
    if (this.api.url.indexOf("http") == -1) {
      if (this.api.url.substring(this.api.url.length - 1) == "2") {
        var uri = this.api.url.substring(0, this.api.url.length - 1);
        this.api.url = `http://newton2.eycproveedores.com/${uri}/public/`;
      } else {
        this.api.url = `http://newton.eycproveedores.com/${this.api.url}/public/`;
      }
    }
    if (this.api.url[this.api.url.length - 1] != "/") {
      this.api.url += "/";
    }
    let loader = this.loading.create({
      content: "Iniciando Sesión...",
      duration: 3000
    });
    loader.present();
    this.api
      .doLogin()
      .then((data: any) => {
        if (data.nombre) {
          this.api
            .get("lang")
            .then(langs => {
              this.api.langs = langs;
            })
            .catch(console.error);
          loader.dismiss().then(() => {
            this.navController.setRoot(HomePage);
            this.events.publish("login", {});
            if (!this.api.user.first_login) {
              this.modal.create("PasswordChangePage").present();
            }
            try {
              this.api.pushRegister();
            } catch (error) {
              console.warn(error);
            }
          });
        } else {
          loader.dismiss().then(() => {
            this.alert.create({ title: "Error", message: "Usuario y Contraseña Invalidos", buttons: ["ok"] }).present();
          });
        }
      })
      .catch((err: any) => {
        console.error(err);
        loader.dismiss().then(() => {
          this.alert.create({ title: "Error", message: "Error al iniciar sesión", buttons: ["ok"] }).present();
        });
      });
  }
}
